terraform {
  source = "${dirname(find_in_parent_folders())}/../../modules/vpc"
}

include {
  path = find_in_parent_folders()
}

locals {
  common-vars  = yamldecode(file("${find_in_parent_folders("common-vars.yaml")}"))
}

inputs = {
  aws_region   = local.common-vars.aws_region
  version      = local.common-vars.version
  profile      = local.common-vars.profile
//  cluster_name = local.common-vars.cluster_name
}

terraform {
  backend "s3" {}
}

provider "aws" {
  region  = var.aws_region
  version = "~> 3.0"
  profile = var.profile
  assume_role {
    role_arn = var.assume_role_arn
  }
}


//noinspection MissingModule
//module "vpc" {
//  source  = "terraform-aws-modules/vpc/aws"
//  version = "2.48.0"
//  name = "vpc-leadiq-gitlab-runner-eks"
//  cidr = "20.20.0.0/16"
//
//  azs                 = ["us-west-2a", "us-west-2b", "us-west-2c", "us-west-2d"]
//  private_subnets     = ["20.20.0.0/20", "20.20.16.0/20", "20.20.32.0/20", "20.20.48.0/20"]
//  public_subnets      = ["20.20.112.0/20", "20.20.128.0/20", "20.20.144.0/20", "20.20.160.0/20"]
//
//  enable_nat_gateway           = true
//  single_nat_gateway           = var.single_nat_gateway
//  enable_dns_hostnames         = true
//  enable_dns_support           = true
//  one_nat_gateway_per_az       = var.one_nat_gateway_per_az
//  public_subnet_tags = {
//    "kubernetes.io/role/elb" = 1
//  }
//
//  private_subnet_tags = {
//    "kubernetes.io/role/internal-elb" = 1
//  }
//
//  tags = {
//    Terraform                                                         = "true"
//    Environment                                                       = "gitlab-runner"
//    Scope                                                             = "gitlab-runner-infrastrucuture"
//    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
//  }
//}